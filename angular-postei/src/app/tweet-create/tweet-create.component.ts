import { Router } from '@angular/router';
import { TweetService } from './../services/tweet.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../post';

@Component({
  selector: 'app-tweet-create',
  templateUrl: './tweet-create.component.html',
  styleUrls: ['./tweet-create.component.css'],
})
export class TweetCreateComponent implements OnInit {
  post: Post;

  constructor(private tweetService: TweetService, private router: Router) {
    this.post = new Post();
  }

  ngOnInit(): void {}

  postTweet() {
    this.post.hashtags = this.post.text.match(/#\w+/mg);

    this.tweetService.postTweet(this.post).subscribe({
      next: (data) => {
        this.router.navigate(['']);
      },
      error: (error) => {
        console.log(error.message);
      },
    });
  }
}

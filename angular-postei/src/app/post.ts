export class Post {
  public id: number;
  public username: string;
  public text: string;
  public hashtags: string[];
  public created_at: string;

  constructor() {}
}

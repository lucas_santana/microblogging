import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TweetCreateComponent} from './tweet-create/tweet-create.component';
import {TweetPostComponent} from './tweet-post/tweet-post.component';

const routes: Routes = [
  {path:'',component:TweetPostComponent},
  {path:'tweet',component:TweetCreateComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

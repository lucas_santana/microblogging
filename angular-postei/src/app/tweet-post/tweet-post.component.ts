import { TweetService } from './../services/tweet.service';
import { Component, OnInit } from '@angular/core';

import { Post } from '../post';

@Component({
  selector: 'app-tweet-post',
  templateUrl: './tweet-post.component.html',
  styleUrls: ['./tweet-post.component.css'],
})
export class TweetPostComponent implements OnInit {
  public posts: Post[];

  constructor(public tweetPostService: TweetService) {}

  ngOnInit(): void {
    this.getTweets();
  }

  deleteTweet(event) {
    const id = event.target.closest('a').id;
    this.tweetPostService.deleteTweet(id).subscribe({
      next: (data) => {
        // Para remover o post da lista após excluido com sucesso
        const i = this.posts.findIndex((p) => p.id === id);
        if (i >= 0) {
          this.posts.splice(i, 1);
        }
        alert('Tweet removido com sucesso!');
      },
      error: (errorInfo) => {
        alert(errorInfo.error);
      },
    });
  }

  getTweetsByTag(searchTag) {
    this.tweetPostService.getTweetsByTag(searchTag).subscribe({
      next: (post) => {
        this.posts = post;
      },
      error: (error) => {
        console.log(error.message);
      },
    });
  }

  getTweets() {
    this.tweetPostService.getTweets().subscribe({
      next: (post) => {
        this.posts = post;
      },
      error: (error) => {
        console.log(error.message);
      },
    });
  }
}

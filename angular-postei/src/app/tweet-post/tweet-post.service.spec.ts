import { TestBed } from '@angular/core/testing';

import { TweetPostService } from './tweet-post.service';

describe('TweetPostService', () => {
  let service: TweetPostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TweetPostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

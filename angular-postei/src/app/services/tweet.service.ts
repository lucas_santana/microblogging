import { Post } from './../post';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TweetService {
  readonly endPoint = 'http://127.0.0.1:8000/api/tweets';

  constructor(private http: HttpClient) {}

  postTweet(post: Post) {
    return this.http.post<Post>(this.endPoint, post);
  }

  getTweets() {
    return this.http.get<Post[]>(this.endPoint);
  }

  deleteTweet(id: number) {
    return this.http.delete(`${this.endPoint}/${id}`);
  }

  getTweetsByTag(search) {
    return this.http.get<Post[]>(`${this.endPoint}/${search}`);
  }
}

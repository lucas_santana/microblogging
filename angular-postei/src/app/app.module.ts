import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { TweetPostComponent } from './tweet-post/tweet-post.component';
import { TweetCreateComponent } from './tweet-create/tweet-create.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    TweetPostComponent,
    TweetCreateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

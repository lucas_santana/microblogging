<?php

use App\Tweet;
use Illuminate\Database\Seeder;

class TweetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Tweet::truncate();

        $faker = \Faker\Factory::create();

        for($i=0;$i<100;$i++){
            Tweet::create(
                [
                    'username'=>$faker->userName,
                    'text'=>$faker->text,
                    'hashtags'=>"#".implode(', #',$faker->words(10))
                ]
                );

        }
    }
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    //
    protected $fillable  = ['username','text','hashtags'];

    public function getCreatedAtAttribute($value)
    {
        $date = Carbon::parse($value);
        return $date->format('d/m/Y H:i');
    }

    public function setHashtagsAttribute($value){
        $this->attributes['hashtags'] = implode(',', $value);
    }
}

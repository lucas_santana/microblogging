<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;

class TweetController extends Controller
{

    public function searchTweetsByHashtag ($search)
    {
        $tweets = Tweet::take(20)->orderBy('created_at', 'desc')->where('hashtags','like',"%$search%")->get();
        return $tweets;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tweets = Tweet::take(20)->orderBy('created_at', 'desc')->get();

        return Tweet::take(20)->orderBy('created_at', 'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tweet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $re = '/#\w+/m';
        $str = $request->input('text');
        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

        $hashtags = collect($matches)->collapse();
        $hashtags = $hashtags->implode(',');
        $request->merge(['hashtags' => $hashtags]); */


        $tweet = Tweet::create($request->all());
        return response()->json($tweet, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tweet $tweet
     * @return \Illuminate\Http\Response
     */
    public function show(Tweet $tweet)
    {
        return $tweet;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tweet $tweet
     * @return \Illuminate\Http\Response
     */
    public function edit(Tweet $tweet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tweet               $tweet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tweet $tweet)
    {
        $tweet = Tweet::findOrFail($tweet->id);
        $tweet->update($request->all());

        return response()->json($tweet, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tweet $tweet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tweet $tweet)
    {
        try {
            $tweet = Tweet::findOrFail($tweet->id);
            $tweet->delete();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $exception) {
            return response()->json("Tweet não encontrado!", 404);
        }

        return 204;
    }
}

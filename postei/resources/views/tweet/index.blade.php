<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>Tweets</title>
</head>
<body>
<div class="col offset-3 col-6">
    <a href="{{route('tweet.create')}}" class="btn btn-primary">Criar Tweet</a>
    @foreach($tweets as $tweet)
        @component('components.tweet',['tweet'=>$tweet])
        @endcomponent
    @endforeach
</div>

<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
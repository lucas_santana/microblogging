<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="{{asset('css/app.css')}}">


    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <form action="{{route('tweet.post')}}" method="post">
                @csrf
                <div class="form-group offset-3 col-6">
                    <label for="username">Username:</label>
                    <input class="form-control" name="username" id="username" type="text"/>
                </div>
                <div class="form-group offset-3 col-6">
                    <label for="text">Texto:</label>
                    <textarea class="form-control" name="text" id="text" cols="30" rows="10"></textarea>
                </div>

                <div class="form-group offset-3 col-6">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                    <a href="{{route('tweet.index')}}" class="btn btn-danger">Cancelar</a>
                </div>
            </form>
        </div>

        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>

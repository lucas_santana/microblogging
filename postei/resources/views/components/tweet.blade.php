
<div class="card m-2">
    <div class="card-header">
        Tweet
    </div>
    <div class="card-body">
        <blockquote class="blockquote mb-0">
            <p>{{ $tweet->text }}</p>
            <footer class="blockquote-footer">{{$tweet->username}}</footer>
        </blockquote>
    </div>
</div>
